FROM gitpod/workspace-full

# Install custom tools, runtimes, etc.
# For example "bastet", a command-line tetris clone:
# RUN brew install bastet
#
# More information: https://www.gitpod.io/docs/config-docker/

USER root
# Install dependencies.
RUN apt-get update \
    && apt-get -y install texlive-base texlive-publishers latexmk \
    && apt-get autoremove -yq \
    && rm -rf /var/lib/apt/lists/* 
RUN texhash

USER gitpod
